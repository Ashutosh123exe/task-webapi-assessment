﻿using Microsoft.AspNetCore.Mvc;
using TaskApi.ITaskRepository;
using TaskApi.Models;
using Task = TaskApi.Models.Task;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TaskApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        // Add required dependencies here
         ITask _repo;
        public TaskController(ITask repo)
        {
            _repo = repo;
        }
        [HttpPost]
        public void AddTask(Task task)
        {
            _repo.AddTask(task);
        }
        [HttpPut("{id}")]
        public void UpdateTask(int id,Task task)
        {
            _repo.Edit(id,task);
        }
        [HttpDelete("{id}")]
        public void DeleteTask(int id)
        {
            _repo.Delete(id);
        }
        [HttpGet("{id}")]
        public Task GetTask(int id)
        {
           return _repo.GetTaskById(id);
        }
        [HttpGet]
        public List<Task> GetAllTask()
        {
           return _repo.GetAllTask();
        }
    }
}
