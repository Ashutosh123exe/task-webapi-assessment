﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TaskApi.ITaskRepository;
using TaskApi.Models;

namespace TaskApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubTaskController : ControllerBase
    {
        // Add required dependencies here
        //public async Task<IActionResult> GetAllSubtasks()
        //{

        //}

        ISubTask _repo;
        public SubTaskController(ISubTask repo)
        {
            _repo = repo;
        }
        [HttpPost]
        public void AddTask(SubTask subTask)
        {
            _repo.AddSubTask(subTask);
        }
        [HttpGet("{id}")]
        public SubTask GetSubTask(int id)
        {
            return _repo.GetSubTask(id);
        }
        [HttpGet]
        public List<SubTask> GetAllSubTask()
        {
            return _repo.GetAllSubTask();
        }
    }
}
