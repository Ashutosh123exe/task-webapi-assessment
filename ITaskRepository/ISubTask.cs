﻿using TaskApi.Models;

namespace TaskApi.ITaskRepository
{
    public interface ISubTask
    {
        public void AddSubTask(SubTask subTask);

        public List<SubTask> GetAllSubTask();

        public SubTask GetSubTask(int id);
    }
}
