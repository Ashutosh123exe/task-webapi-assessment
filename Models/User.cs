﻿using System.ComponentModel.DataAnnotations;

namespace TaskApi.Models
{
    public class User
    {
        [Key]
        public string UserName { get; set; }

        public string Password { get; set; }
    }
}
