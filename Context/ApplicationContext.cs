﻿using Microsoft.EntityFrameworkCore;
using TaskApi.Models;
using Task = TaskApi.Models.Task;

namespace TaskApi.Context
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext()
        {
            
        }
        public ApplicationContext (DbContextOptions<ApplicationContext> options) : base(options) { }

        public DbSet<Task> Tasks { get; set; }

        public DbSet<SubTask> SubTasks { get; set; }

        public DbSet<User> users { get; set; } 
    }
}
