﻿using TaskApi.Context;
using TaskApi.ITaskRepository;
using TaskApi.Models;
using Task = TaskApi.Models.Task;

namespace TaskApi.TaskRepository
{
    public class SubTaskRepository : ISubTask
    {
        ApplicationContext _dbcontext;
        public SubTaskRepository(ApplicationContext context)
        {
            _dbcontext = context;
        }
        
        public void AddSubTask(SubTask subTask)
        {
            Task task = _dbcontext.Tasks.Where(x=>x.Id == subTask.Task.Id).FirstOrDefault();
            subTask.Task = task;
            if(subTask != null)
                _dbcontext.Add(subTask);
                _dbcontext.SaveChanges();
        }

        public SubTask GetSubTask(int id)
        {
            SubTask subTask = _dbcontext.SubTasks.Where(x=>x.Id == id).FirstOrDefault();
            return subTask;
        }

        public List<SubTask> GetAllSubTask()
        {
            var list = _dbcontext.SubTasks.ToList();
            return list;
        }
    }
}
