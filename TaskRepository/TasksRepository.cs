﻿using TaskApi.Context;
using TaskApi.ITaskRepository;
using TaskApi.Models;
using Task = TaskApi.Models.Task;

namespace TaskApi.TaskRepository
{
    public class TasksRepository : ITask
    {
        ApplicationContext _dbContext;
        public TasksRepository(ApplicationContext dbContext)
        {
            _dbContext = dbContext;
        }
        public void AddTask(Task task)
        {
            if(task!=null)
            _dbContext.Tasks.Add(task);
            _dbContext.SaveChanges();
           

        }

        public void Delete(int id)
        {
            SubTask subTask = _dbContext.SubTasks.Where(x => x.TaskId == id).FirstOrDefault();
            if (subTask == null)
            {
                Models.Task task = _dbContext.Tasks.FirstOrDefault(t => t.Id == id);
                _dbContext.Remove(task);
            }

            _dbContext.SaveChanges();
        }

        public void Edit(int id, Task task)
        {
            Task task1 = _dbContext.Tasks.Find(task.Id);
            if(task!=null)
            {
                task1.Name= task.Name;
                task1.CreatedOn = task.CreatedOn;
                task1.CreatedBy= task.CreatedBy;
                task1.Descriprion=task.Descriprion;
            }
            _dbContext.Tasks.Update(task1);
        }

        public List<Task> GetAllTask()
        {
            var Tasks = _dbContext.Tasks.ToList();
            return Tasks;
        }

        public Task GetTaskById(int id)
        {
            Task task = _dbContext.Tasks.Where(x=>x.Id==id).FirstOrDefault();
            return task;
        }
    }
}
